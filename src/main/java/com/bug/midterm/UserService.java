/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bug.midterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author OS
 */
public class UserService {
    private static ArrayList<Mainproduct> MainList = new ArrayList<>();
    static {
        load();
    }
    
    public static boolean additem(Mainproduct item){
        MainList.add(item);
        save();
        return true;
    }
    public static boolean delitem(Mainproduct user) {
        MainList.remove(user);
        save();
        return true;
    }

    public static boolean delitem(int index) {
        MainList.remove(index);
        save();
        return true;
    }

    public static ArrayList<Mainproduct> getitem() {
        return MainList;
    }

    public static Mainproduct getitem(int index) {
        return MainList.get(index);
    }

    public static boolean updateitem(int index, Mainproduct item) {
        MainList.set(index, item);
        save();
        return true;
    }
    
    public static boolean clearAll(){
        MainList.clear();
        save();
        return true;
    }
    
    public static double totalPrice(){
        int sum = 0;
        for(int i  = 0;i<MainList.size();i++){
            sum+=(MainList.get(i).getPrice()*MainList.get(i).getAmount());
        }
        return sum;
    }
    
    public static int totalAmount(){
        int sum = 0;
        for(int i = 0;i<MainList.size();i++){
            sum+=MainList.get(i).getAmount();
        }
        return sum;
    }

    public static void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("M.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(MainList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("M.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            MainList = (ArrayList<Mainproduct>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
